// src/main.js  
const express = require('express');
const mylib = require('./mylib');
const app = express();
const port = 3000;

app.get('/', (req, res) =>{
    res.send('Hello World');
});
app.get('/add', (req, res) =>{
    const a = parseInt(req.query.a);
    const b =parseInt(req.query.b);
    console.log({a,b});
    const sum =mylib.add(a, b); //calling funtion from the other file
    res.send(sum.toString());
});
app.get('/substract',(req, res) =>{
    const a =parseInt(req.query.a);
    const b =parseInt(req.query.b);
    console.log({a,b});
    const substract=mylib.sub(a,b);
    res.send(substract.toString());
});
app.listen(port, () =>{
    console.log(`Server:http://localhost:${port}`)
});