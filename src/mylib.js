/**
 * 
 * This arrow function returns the sum of two parameters
 * @param {number} param1 this is the frist parameter 
 * @param {number} param2  this is the second parameter
 * @returns {number}
 */
const add =(param1, param2) =>{
    const result = param1 +param2;
    return result
}
/**
 * This is arrow funtion too. It it on single line , it has return statment
 * Without curly brackets it doesn't require return keyword.
 * @param {number} a frist number    
 * @param {number} b second number
 * @returns 
 */
const subtract =(a, b) => a-b;

module.exports= {
    add,
    sub: subtract,
    random: () => Math.random(),
    arrayGen: () => [1,2,3]
}
