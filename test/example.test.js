// example.test.js
const expect = require('chai').expect;
const assert = require('chai').expect;
const mylib = require('../src/mylib');
var should = require('should');
describe("Unit testing mylib.js", () => {
    let myvar = undefined;

    after(() => console.log('After mylib tests'));

    it("Should return 2 when using sum funtion with parameters a=1, b=1", () => {
        const result = mylib.add(1, 1);
        expect(result).to.equal(2);
    })
    it("Should return 3 when using function with parameters a=5, b=2",() =>{
        const result =mylib.sub(5,2);
        expect(result).to.equal(3);
    })


    before(() => {
        myvar = 1;
        console.log('Before testing');
    });

    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar');
    })
    it('Myvar should exist', () => {
        should.exist(myvar);
    })
});